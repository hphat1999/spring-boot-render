package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {
    private String type;
    private String id;
    private String text;
}
