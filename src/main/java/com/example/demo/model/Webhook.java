package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Webhook {
    private String destination;

    private List<Events> events;
}
