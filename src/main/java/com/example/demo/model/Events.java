package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Events {
    private String type;

    private Message message;

    private String webhookEventId;

    private DeliveryContext deliveryContext;

    private Long timestamp;

    private Source source;

    private String replyToken;

    private String mode;
}
