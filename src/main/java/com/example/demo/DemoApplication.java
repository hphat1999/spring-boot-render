package com.example.demo;

import com.example.demo.model.Webhook;
import com.example.demo.service.WebhookService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

@SpringBootApplication
@RestController
public class DemoApplication {

    private final String TYPE_WEBHOOK_MESSAGE = "message";


    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @PostMapping("/webhook")
    public void getRequestWebhook(
            @RequestHeader(name = "x-line-signature") String keySignature,
            @RequestBody String header)
            throws NoSuchAlgorithmException, InvalidKeyException {
        String channelSecret = "01b1da5264faeb470b1677414883e363";
        SecretKeySpec key = new SecretKeySpec(channelSecret.getBytes(), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(key);
        System.out.println(keySignature);
        System.out.println(1);
        System.out.println(header);
        byte[] source = header.getBytes(StandardCharsets.UTF_8);
        String signature = Base64.encodeBase64String(mac.doFinal(source));
        if ("Ud3184a7e22f37a39dd1c11b0479f7881".equals(keySignature)) {
            Gson gson = new Gson();
            Webhook webhook = gson.fromJson(header, Webhook.class);
            handleWebhookLine(webhook);
        }
    }

    @PostMapping("/webhook2")
    public void getRequestWebhook2(
        @RequestHeader(name = "x-line-signature") String keySignature,
        @RequestBody String header)
        throws NoSuchAlgorithmException, InvalidKeyException {
        String channelSecret = "0618512ec828b23c1f6ac64b03ce1d85";
        SecretKeySpec key = new SecretKeySpec(channelSecret.getBytes(), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(key);
        System.out.println(keySignature);
        System.out.println(2);
        System.out.println(header);
        byte[] source = header.getBytes(StandardCharsets.UTF_8);
        String signature = Base64.encodeBase64String(mac.doFinal(source));
        if ("Ud3184a7e22f37a39dd1c11b0479f7881".equals(keySignature)) {
            Gson gson = new Gson();
            Webhook webhook = gson.fromJson(header, Webhook.class);
            handleWebhookLine(webhook);
        }
    }

    @PostMapping("/webhook3")
    public void getRequestWebhook3(
        @RequestHeader(name = "x-line-signature") String keySignature,
        @RequestBody String header)
        throws NoSuchAlgorithmException, InvalidKeyException {
        String channelSecret = "adfcdd7d7a9d89c244b085073c9212b9";
        SecretKeySpec key = new SecretKeySpec(channelSecret.getBytes(), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(key);
        System.out.println(keySignature);
        System.out.println(3);
        System.out.println(header);
        byte[] source = header.getBytes(StandardCharsets.UTF_8);
        String signature = Base64.encodeBase64String(mac.doFinal(source));
        if ("Ud3184a7e22f37a39dd1c11b0479f7881".equals(keySignature)) {
            Gson gson = new Gson();
            Webhook webhook = gson.fromJson(header, Webhook.class);
            handleWebhookLine(webhook);
        }
    }

    @PostMapping("/webhook4")
    public void getRequestWebhook4(
        @RequestHeader(name = "x-line-signature") String keySignature,
        @RequestBody String header)
        throws NoSuchAlgorithmException, InvalidKeyException {
        String channelSecret = "ae746821bc49e855856081de5f0c0b30";
        SecretKeySpec key = new SecretKeySpec(channelSecret.getBytes(), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(key);
        System.out.println(keySignature);
        System.out.println(4);
        System.out.println(header);
        byte[] source = header.getBytes(StandardCharsets.UTF_8);
        String signature = Base64.encodeBase64String(mac.doFinal(source));
        if ("Ud3184a7e22f37a39dd1c11b0479f7881".equals(keySignature)) {
            Gson gson = new Gson();
            Webhook webhook = gson.fromJson(header, Webhook.class);
            handleWebhookLine(webhook);
        }
    }



    private void handleWebhookLine(Webhook webhook) {
        if (TYPE_WEBHOOK_MESSAGE.equals(webhook.getEvents().get(0).getType())) {
            System.out.println(webhook.getEvents().get(0).getType());
            System.out.println(webhook.getEvents().get(0).getSource().getUserId());
            System.out.println(webhook.getEvents().get(0).getMessage().getId());
            System.out.println(webhook.getEvents().get(0).getMessage().getText());
            System.out.println("-------------------------------------------------");
        }
    }
}
